import sys, os, re


class Player:
	def __init__(self, name, bats, hits, runs):
		self.name = name
		self.bats = bats
		self.hits = hits
		self.rate = rate


if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])
	
pattern = re.compile(r"^([a-zA-Z]+ [a-zA-Z]+) batted (\d+) times with (\d+) hits and (\d+) runs$")

dict = {}
f = open(filename)
for line in f:
	data = pattern.match(line.rstrip())
	if data is None:
		continue
	entry = data.groups()
	name = entry[0]
	bats = float(entry[1])
	hits = float(entry[2])

	if name in dict:
		tup = dict[name]
		dict[name] = (tup[0] + bats, tup[1] + hits)
	else:
		dict[name] = (bats, hits)
f.close();

result = []
for name in dict:
	bats, hits = dict[name]
	rate = hits / bats
	result.append(Player(name, bats, hits, rate))
	
result.sort(key = lambda x: x.rate, reverse = True)
for p in result:
    print "%s: %1.3f" % (p.name, p.rate)
	





